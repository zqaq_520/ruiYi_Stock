import store from '@/vuex'

export function clearvuex(obj) {
  store.commit('updateLocalUserInfo',{})
  store.commit('updateTablesOfAll',[])
  store.commit('updateBusinessHours',{})
  store.commit('updateSubscribe_roomTypeList',[])

  store.commit('updateAuthority',{})
  store.commit('updateAuthoritySubscribe',{})

  store.commit('updateSalesPresentList',[])
  store.commit('updateSalesMaxNumber',0)
  store.commit('updateKeepPresentList',[])
  store.commit('updateGirlPresentList',[])
  store.commit('updateSalesSelectedList',[])
  store.commit('updateKeepSelectedList',[])
  store.commit('updateGirlSelectedList',[])

  if(obj != undefined && obj.login){
    store.dispatch('fetchAuthority')
  }
}
