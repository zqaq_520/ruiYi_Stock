
const state = {//数据
  salesPresentList: [],
  salesMaxNumber:0,
  keepPresentList: [],
  girlPresentList: [],
  salesSelectedList: [],
  keepSelectedList: [],
  girlSelectedList: []
}

const getters = {//获取数据
  /*getSalesPresentList: function (state) {
    return state.salesPresentList
  }*/
  getSalesPresentList: state => state.salesPresentList,//es6箭头函数写法
  getSalesMaxNumber: state => state.salesMaxNumber,
  getKeepPresentList: state => state.keepPresentList,
  getGirlPresentList: state => state.girlPresentList,
  getSalesSelectedList: state => state.salesSelectedList,
  getKeepSelectedList: state => state.keepSelectedList,
  getGirlSelectedList: state => state.girlSelectedList
}

const mutations = {//对本地无需ajax的更改，使用mutations
  updateSalesPresentList (state, changed) {
    state.salesPresentList = changed
  },
  updateSalesMaxNumber (state, changed) {
    state.salesMaxNumber = changed
  },
  updateKeepPresentList (state, changed) {
    state.keepPresentList = changed
  },
  updateGirlPresentList (state, changed) {
    state.girlPresentList = changed
  },
  updateSalesSelectedList (state, changed) {
    state.salesSelectedList = changed
  },
  updateKeepSelectedList (state, changed) {
    state.keepSelectedList = changed
  },
  updateGirlSelectedList (state, changed) {
    state.girlSelectedList = changed
  },
}

const actions = {//action好处适合异步操作，action先执与后端交互，mutations后执行
}

export default {
  state,
  getters,
  mutations,
  actions
}
