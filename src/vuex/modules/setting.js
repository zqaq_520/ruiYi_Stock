
const state = {//数据
  isLoading: true,
  localUserInfo: {},
  tablesOfAll: [],
  businessHours:{},
  subscribe_roomTypeList:[],
  unpayedPresentList:[]
}

const getters = {//获取数据
  /*getLoadingStatus: function (state) {
    return state.isLoading
  }*/
  getLoadingStatus: state => state.isLoading,//es6箭头函数写法
  getLocalUserInfo: state => state.localUserInfo,
  getTablesOfAll: state => state.tablesOfAll,
  getBusinessHours: state => state.businessHours,
  getSubscribe_roomTypeList: state => state.subscribe_roomTypeList,
  getUnpayedPresentList: state => state.unpayedPresentList
}

const mutations = {//对本地无需ajax的更改，使用mutations
  updateLoadingStatus (state, changed) {
    state.isLoading = changed.isLoading
  },
  updateLocalUserInfo (state, changed) {
    state.localUserInfo = changed
  },
  updateTablesOfAll (state, changed) {
    state.tablesOfAll = changed
  },
  updateBusinessHours(state,changed){
    state.businessHours = changed
  },
  updateSubscribe_roomTypeList(state,changed){
    state.subscribe_roomTypeList = changed
  },
  updateUnpayedPresentList(state,changed){
    state.unpayedPresentList = changed
  }
}

const actions = {//action好处适合异步操作，action先执与后端交互，mutations后执行
}

export default {
  state,
  getters,
  mutations,
  actions
}
