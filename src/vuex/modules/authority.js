
import {getAuthority} from '../../api'
import {message} from "../../common/util";


const state = {//数据
  authority: {},
  authoritySubscribe:{}
}

const getters = {//获取数据
  /*getAuthority: function (state) {
    return state.authority
  }*/
  getAuthority: state => state.authority,//es6箭头函数写法
  getAuthoritySubscribe: state => state.authoritySubscribe
}

const mutations = {//对本地无需ajax的更改，使用mutations
  updateAuthority(state,changed){
    state.authority = changed
  },
  updateAuthoritySubscribe(state,changed){
    state.authoritySubscribe = changed
  }

}

const actions = {//action好处适合异步操作，action先执与后端交互，mutations后执行
  fetchAuthority({commit,state},params){
    getAuthority().then((res)=>{
      if(res.status == 'success')  {
        commit('updateAuthority',res.data)
        let subscribeManagerValue = {viewValue:'self', hostessValue:''}
        for(let authItem of res.data.h5_stockHelper){
          if(authItem.subscribeManager_stock != undefined){
            for(let item of authItem.subscribeManager_stock){
              if(typeof item == 'string' && item == 'subscribe_hostess'){
                subscribeManagerValue.hostessValue = 'manager'
              }
              if(typeof item == 'string' && item == 'subscribe_manager'){
                subscribeManagerValue.viewValue = 'manager'
              }else if(typeof item == 'string' && item == 'subscribe_depart' && subscribeManagerValue.viewValue != 'manager'){
                subscribeManagerValue.viewValue = 'depart'
              }
            }
          }
        }
        //console.log(subscribeManagerValue)
        commit('updateAuthoritySubscribe',{view:subscribeManagerValue.viewValue,hostess:subscribeManagerValue.hostessValue})
       }else if(res.status == 'error'){
        console.log('getAuthority error info:',res.msg)
        message({message:res.msg,type: 'error'})
      }
      if(params.callback != undefined)params.callback(res.status)
    }).catch((err)=>{console.log('getAuthority catch info:',err)})
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
