import Vue from 'vue'
import Vuex from 'vuex'
import setting from './modules/setting'
import authority from './modules/authority'
import presentCar from './modules/presentCar'

Vue.use(Vuex)

export default new Vuex.Store({
  modules:{
    setting,
    authority,
    presentCar
  }
})
