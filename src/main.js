// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { XHeader, WechatPlugin, ToastPlugin, ConfirmPlugin, AlertPlugin } from 'vux'
import mock from './mock'
import store from './vuex'
import './assets/iconfont/iconfont.css'
//if(process.env.NODE_ENV === 'development')
if(JSON.parse(localStorage.getItem('debugMode'))){
  require('./vconsole/vconsole')
}
/*const FastClick = require('fastclick')
FastClick.attach(document.body)*/
import Global_ from './common/global'

Vue.config.productionTip = false

Vue.use(WechatPlugin)
Vue.use(ConfirmPlugin)
Vue.use(ToastPlugin)
Vue.use(AlertPlugin)
Vue.component('x-header', XHeader)

/* eslint-disable no-new */
const vm = new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})

Vue.prototype.GLOBAL = Global_ //挂载到Vue实例上面

export default vm
