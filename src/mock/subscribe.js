import Mock from 'mockjs'
import {dateFormat} from 'vux'

const subscribeTotals = {
  status:'success',
  data:[
    {name:'包厢',total:'@integer(1, 5)'},
    {name:'高卡',total:'@integer(1, 10)'},
    {name:'卡座',total:'@integer(10, 20)'},
    {name:'散台',total:'@integer(20, 50)'}
  ]
}

const subscribeItem = {
  status:'success',
  data:{
    CREATE_DATETIME:'@datetime("yyyy-MM-dd HH:mm")',
    SUBSCRIBE_ID: '@string(32)',
    SUBSCRIBE_NUMBER:'20181212100'+'@integer(100000000, 999999999)',
    SUBSCRIBER: '@cname',
    'subscribe_DEPARTMENT|1':['营销部','市场部','楼面部','演义部','工程部'],
    'SUBSCRIBE_TYPE|1':[0,1],//0-普通 1-节日派对
    'SUBSCRIBE_MODE|1':[1,2,3,4],//1-自助预约 2-销售预约 3-老板预约 4-股东预约
    'PHONE|1': ['13567686796','13756462564','13354657567'],
    CUSTOMER_NAME: '@cname',
    CONSUMPTION_NUMBER: '@integer(1, 10)',
    ROOMPLATFORMTYPE_ID: '@string(32)',
    'SUBSCRIBE_ROOM_PLATFORM_TYPE|1': ['散台','卡座','高卡','包厢'],
    'ROOM_PLATFORM_TYPE|1': ['','散台','卡座','高卡','包厢'],
    'AREA|1':['','大厅区','吧台区','包厢区'],
    'NUMBER|1':['','A1台','B1台','C1台','M1台','A2台','B2台','C2台','M2台','A3台','B3台','C3台','M3台','1号房'],
    STATE: '@integer(0, 9)',//预约状态 0-正在预约 1-成功 2-失败 3-取消 4-已到店 5-超时 6-已开单 7-已消费 8-未付押金 9-已结账
    SUBSCRIBE_DATETIME:dateFormat(new Date().setDate(new Date().getDate()+30), 'YYYY-MM-DD HH:mm'),
    'REMARK|1':['','@csentence'],
    'DEPOSIT|1':['0','@float(1000, 10000, 2, 2)'],
    'IS_DEPOSIT|1':[0,1],//是否需要缴纳押金0-否 1-是
    'FAIL_REASON|1':['@csentence'],
    'CANCEL_REASON|1':['@csentence']
  }
}

const subscribeIdList = {
  status: 'success',
  'data|19': [
    {
      SUBSCRIBE_ID: '@string(32)'
    }
  ]
}

const subscribeList = {
  status:'success',
  'data|8':[
    {
      SUBSCRIBE_ID: '@string(32)',
      ORDER_ID: '@string(32)',
      SUBSCRIBE_NUMBER:'20181212100'+'@integer(100000000, 999999999)',
      'SUBSCRIBE_MODE|1':[1,2,3,4],//1-自助预约 2-销售预约 3-老板预约 4-股东预约
      SUBSCRIBER: '@cname',
      'subscribe_DEPARTMENT|1':['营销部','市场部','楼面部','演义部','工程部'],
      'PHONE|1': ['13567686796','13756462564','13354657567'],
      CUSTOMER_NAME: '@cname',
      CONSUMPTION_NUMBER: '@integer(1, 10)',
      'SUBSCRIBE_ROOM_PLATFORM_TYPE|1': ['散台','卡座','高卡','包厢'],
      'ROOM_PLATFORM_TYPE|1': ['','散台','卡座','高卡','包厢'],
      'AREA|1':['','大厅区','吧台区','包厢区'],
      'NUMBER|1':['','A1台','B1台','C1台','M1台','A2台','B2台','C2台','M2台','A3台','B3台','C3台','M3台','1号房'],
      STATE: '@integer(0, 9)',//预约状态 0-正在预约 1-成功 2-失败 3-取消 4-已到店 5-超时 6-已开单 7-已消费 8-未付押金 9-已结账
      SUBSCRIBE_DATETIME:'@datetime("yy-MM-dd HH:mm")',
      'REMARK|1':['','@csentence'],
      'saled|1':[0,'@float(1000, 10000, 2, 2)'],
      'presented|1':[0,'@float(1000, 10000, 2, 2)'],
    }
  ]
}

const subscribeStatus = {
  status:'success',
  data:{
    payOrNot:Mock.Random.boolean(),
    SUBSCRIBE_ID: '@string(32)'
  },
  msg:'后端返回的提示信息'
}

const holidayPartyItem = {
  status:'success',
  data: {
    HOLIDAYPARTY_ID: '@string(32)',
    PARTY_NAME:'@csentence',
    START_DATETIME:dateFormat(new Date().setDate(new Date().getDate()+30), 'YYYY-MM-DD HH:mm'),
    END_DATETIME:dateFormat(new Date().setDate(new Date().getDate()+33), 'YYYY-MM-DD HH:mm'),
    'STATE|1':[0,1,2],//0-预约中 1-结束 2-停用
    DESCRIPTION:'<img src="/assets/images/test/testMealItem1.jpg"/>'+'@cparagraph()'+'<img src="/assets/images/test/testMealItem2.jpg"/>'+'@cparagraph()',
    CREATE_DATETIME:dateFormat(new Date().setDate(new Date().getDate()-10), 'YYYY-MM-DD HH:mm')
  }
}

const holidayPartyList = {
  status:'success',
  'data|5':[
    {
      HOLIDAYPARTY_ID: '@string(32)',
      PARTY_NAME:'@csentence',
      START_DATETIME:dateFormat(new Date().setDate(new Date().getDate()+30), 'YYYY-MM-DD HH:mm'),
      END_DATETIME:dateFormat(new Date().setDate(new Date().getDate()+33), 'YYYY-MM-DD HH:mm'),
      'STATE|1':[0,1,2],//0-预约中 1-结束 2-停用
      CREATE_DATETIME:dateFormat(new Date().setDate(new Date().getDate()-10), 'YYYY-MM-DD HH:mm'),
      'IMAGE|1':['/assets/images/test/testMealItem1.jpg','/assets/images/test/testMealItem2.jpg','/assets/images/test/testMealItem3.jpg','/assets/images/test/testMealItem4.jpg','/assets/images/test/testMealItem5.jpg']
    }
  ]
}

const depositAbout = {
  status:'success',
  data:{
    deposit:500,
    description:'1.该订金仅用于本次预订使用，预订成功后将用于抵扣本次预订到店消费款；<br/>2.在到达预约时间前未到店，订金将不予退还；'
  }
}

const depositTerm = {
  status:'success',
  data:{
    pdfjsPath:'/assets/pdfjs',
    DepositTermPath:'/assets/testPDF.pdf'
  }
}

const cancelSubscribeStatus = {
  status:'success',
  msg:'后端返回的提示信息'
}

const subscribePerformance = {
  status:'success',
  data:{
    successSubscribe:'@integer(10,30)',
    totalSubscribe:'@integer(20,40)',
    relativeRatio:'@float(0, 0.99, 2, 2)',
    cancelSubscribe:'@integer(1,10)',
    morethan1Visitor:'@integer(1,10)',
    newVisitor:'@integer(1,10)',
    frequentVisitor:'@float(0, 0.99, 2, 2)',
    'detail|3-10':[
      {
        subscriber:'@cname()',
        subscribeNum:'@integer(1,10)',
        presentTotal:'@float(1000, 10000, 2, 2)',
        consumption:'@float(1000, 10000, 2, 2)'
      }
    ]
  }
}

export {subscribeTotals,subscribeItem,subscribeIdList,subscribeList,subscribeStatus,holidayPartyItem,holidayPartyList,depositAbout,depositTerm,cancelSubscribeStatus,subscribePerformance}
