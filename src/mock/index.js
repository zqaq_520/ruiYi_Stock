import {contextPath} from "../api";
import Mock from 'mockjs'
import qs from 'qs'
import {authority} from './authority'
import {businessHours} from './setting'
import {loginUser,weiXinAuthStatus,loginSuccuss,loginError,loginOutStatus,userInfo,checkCode,thePassword,checkRegister} from './user'
import {subscribeTotals,subscribeItem,subscribeIdList,subscribeList,subscribeStatus,holidayPartyItem,holidayPartyList,depositAbout,depositTerm,cancelSubscribeStatus,subscribePerformance} from "./subscribe";
import {roomTypeList,tablesByType} from './tableManager'
import {orderDetail,orderIdList,orderList} from './orderManager'
import {choosePresentList,presentStatus,presentIdList,presentList,presentDetail,unpayedPresentList} from './presentManager'
import {checkStockStatus} from './goodManager'
import {turnoverInfo} from './statistics'

// 通过axios与后台交互，使用mock.js来拦截axios造假数据
if(contextPath.indexOf('https') <= 0 && contextPath == ''){//判断是否为开发调试环境
  Mock.mock(/weiXinAuth/,function(options) {
    //console.log('weiXinAuth的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(weiXinAuthStatus)
  })

  Mock.mock(/loginVerify/,function(options) {
    //console.log('loginVerify的axios传值为：',qs.parse(options.body))//数据请求的传值
    let {username, password} = qs.parse(options.body)
    if((username == loginUser.phone || username == loginUser.workId) && password == loginUser.password){
      return Mock.mock(loginSuccuss)
    }else{
      return loginError
    }
  })

  Mock.mock(/loginOut/,function(options) {
    //console.log('loginOut的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(loginOutStatus)
  })

  Mock.mock(/getUserInfo/,function(options) {
    //console.log('getUserInfo的axios传值为：',qs.parse(options.body))//数据请求的传值
    if(qs.parse(options.body).USER_ID != '' && qs.parse(options.body).USER_ID != undefined)
      return Mock.mock(userInfo)
  })

  Mock.mock(/sendCheckCode/,function(options) {
    //console.log('sendCheckCode的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(checkCode)
  })

  Mock.mock(/changePassword/,function(options) {
    //console.log('changePassword的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(thePassword)
  })

  Mock.mock(/getAuthority/,function(options) {
    //console.log('getAuthority的axios传值为：',qs.parse(options.body))//数据请求的传值
    if(qs.parse(options.body).USER_ID != '' && qs.parse(options.body).USER_ID != undefined)
      return Mock.mock(authority)
  })

  Mock.mock(/getBusinessHours/,function(options) {
    //console.log('getBusinessHours的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(businessHours)
  })

  Mock.mock(/getSubscribeTotals/,function(options) {
    //console.log('getSubscribeTotals的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(subscribeTotals)
  })

  Mock.mock(/getSubscribeIdList/,function(options) {
    //console.log('getSubscribeIdList的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(subscribeIdList)
  })

  Mock.mock(/getSubscribeByIds/,function(options) {
    //console.log('getSubscribeByIds的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(subscribeList)
  })

  Mock.mock(/getRoomType/,function(options) {
    //console.log('getRoomType的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(roomTypeList)
  })

  Mock.mock(/checkRegistered/,function(options) {
    //console.log('checkRegistered的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(checkRegister)
  })

  Mock.mock(/submitSubscribe/,function(options) {
    //console.log('submitSubscribe的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(subscribeStatus)
  })

  Mock.mock(/getHolidayPartyById/,function(options) {
    //console.log('getHolidayPartyById的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(holidayPartyItem)
  })

  Mock.mock(/getHolidayPartyList/,function(options) {
    //console.log('getHolidayPartyList的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(holidayPartyList)
  })

  Mock.mock(/getSubscribeById/,function(options) {
    //console.log('getSubscribeById的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(subscribeItem)
  })

  Mock.mock(/getDepositAbout/,function(options) {
    //console.log('getDepositAbout的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(depositAbout)
  })

  Mock.mock(/getDepositTerm/,function(options) {
    //console.log('getDepositTerm的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(depositTerm)
  })

  Mock.mock(/cancelSubscribe/,function(options) {
    //console.log('cancelSubscribe的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(cancelSubscribeStatus)
  })

  Mock.mock(/getOrderDetail/,function(options) {
    //console.log('getOrderDetail的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(orderDetail)
  })

  Mock.mock(/getOrderIdList/,function(options) {
    //console.log('getOrderIdList的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(orderIdList)
  })

  Mock.mock(/getOrderByIds/,function(options) {
    //console.log('getOrderByIds的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(orderList)
  })

  Mock.mock(/getChoosePresentList/,function(options) {
    //console.log('getChoosePresentList的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(choosePresentList)
  })

  Mock.mock(/checkStock/,function(options) {
    //console.log('checkStock的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(checkStockStatus)
  })

  Mock.mock(/submitPresent/,function(options) {
    //console.log('submitPresent的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(presentStatus)
  })

  Mock.mock(/getPresentedIdList/,function(options) {
    //console.log('getPresentedIdList的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(presentIdList)
  })

  Mock.mock(/getPresentByIds/,function(options) {
    //console.log('getPresentByIds的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(presentList)
  })

  Mock.mock(/getPresentDetail/,function(options) {
    //console.log('getPresentDetail的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(presentDetail)
  })

  Mock.mock(/getUnpayedPresent/,function(options) {
    //console.log('getUnpayedPresent的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(unpayedPresentList)
  })

  Mock.mock(/getTablesByType/,function(options) {
    //console.log('getTablesByType的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(tablesByType)
  })

  Mock.mock(/getTurnover/,function(options) {
    //console.log('getTurnover的axios传值为：',qs.parse(options.body))//数据请求的传值
    return Mock.mock(turnoverInfo)
  })

}
