import Mock from 'mockjs'

const choosePresentList = {
  status: 'success',
  data:{
    maxNumber:10,
    list: function(){
      let array = [],nameArray = ['洋酒','啤酒','小吃','软饮','洋酒1','啤酒2','小吃3','软饮4']
      for(var i = 0; i < nameArray.length; i++){
        array.push({GOODSTYPE_ID:'goodTypeid' + i,NAME:nameArray[i],list:[]})
        for(var j = 0; j < 6; j++){
          let randomGoodList = []
          if(Mock.Random.boolean()){
            for(var k = 0; k < Mock.Random.integer(0, 3); k++){
              let randomItem = Mock.mock({
                GOODS_ID: '@string(32)',
                NAME:'@ctitle(5,20)',
                IMAGE: '/assets/images/test/testMealItem'+ Mock.Random.integer(1, 5) +'.jpg',
                'PRICE|500-10000.2': 1,
                'QUANTITY': '@integer(1,10)',
                'UNIT|1':['支','甁','罐','套','盘']
              })
              randomGoodList.push(randomItem)
            }
          }
          let randomMatchDrink = []
          for(var k = 0; k < Mock.Random.integer(1, 10); k++){
            let randomItem = Mock.mock({
              GOODS_ID: '@string(32)',
              MATCHINGDRINK_NAME:'@ctitle(5,20)',
              MATCHINGDRINK_ID: '@string(32)',
              MATCHINGDRINK_NUM: '@integer(8,20)'
            })
            randomMatchDrink.push(randomItem)
          }
          array[i].list.push(Mock.mock({
            GOODS_ID: 'goodid20181005' + j + array[i].GOODSTYPE_ID,
            NAME : '@ctitle(5,20)',
            'PICK_MAX_NUM|1':[randomGoodList.length,randomGoodList.length > 1?randomGoodList.length - 1:randomGoodList.length],
            GOODS_LIST: randomGoodList,
            'MATCHINGDRINK|1':[JSON.stringify(randomMatchDrink),''],
            'IMAGE|1':['/assets/images/test/testMealItem1.jpg','/assets/images/test/testMealItem2.jpg','/assets/images/test/testMealItem3.jpg','/assets/images/test/testMealItem4.jpg','/assets/images/test/testMealItem5.jpg'],
            'PRICE|500-10000.2': 1,
            'UNIT|1':['支','甁','罐','套','盘']
          }))
        }
      }
      return array
    }
  }
}

const presentStatus = {
  status:'success',
  data:{
    PRESENT_ID: '@string(32)'
  },
  msg:'后端返回的提示信息'
}

const presentIdList = {
  status: 'success',
  'data|19': [
    {
      PRESENT_ID: '@string(32)',
      'STATE|1':[0,1,2],//0-待赠送 1-成功赠送 2-不同意赠送
    }
  ]
}

const presentList = {
  status:'success',
  'data|8':[
    {
      PRESENT_ID: '@string(32)',
      ORDER_ID: '@string(32)',
      PRESENTER: '@cname',
      'present_DEPARTMENT|1':['营销部','市场部','楼面部','演义部','工程部'],
      'REMARK|1':['','@csentence'],
      REQUEST_DATETIME:'@datetime("yyyy-MM-dd HH:mm")',
      AUDIT_DATETIME:'@datetime("yyyy-MM-dd HH:mm")',
      'STATE|1':[0,1,2],//0-待赠送 1-成功赠送 2-不同意赠送
      'TYPE|1':[0,1,2,3,4],//赠送类型  0-售卖库存1-存酒库存 2-过期 4-卡赠 3-美女台赠
      AUDITOR: '@cname'
    }
  ]
}

const presentDetail = {
  status:'success',
  data:{
    PRESENT_ID: '@string(32)',
    ORDER_ID: '@string(32)',
    ORDER_NUMBER:'20181212100'+'@integer(100000000, 999999999)',
    payed:'@float(300, 50000, 2, 2)',
    'CUSTOMER_NAME|1':['','@cname'],
    'ROOM_PLATFORM_TYPE|1': ['散台','卡座','高卡','包厢'],
    'AREA|1':['大厅区','吧台区','包厢区'],
    'NUMBER|1':['A1台','B1台','C1台','M1台','A2台','B2台','C2台','M2台','A3台','B3台','C3台','M3台','1号房'],
    PRESENTER: '@cname',
    'present_DEPARTMENT|1':['营销部','市场部','楼面部','演义部','工程部'],
    'REMARK|1':['','@csentence'],
    REQUEST_DATETIME:'@datetime("yyyy-MM-dd HH:mm")',
    AUDIT_DATETIME:'@datetime("yyyy-MM-dd HH:mm")',
    'STATE|1':[0,1,2],//0-待赠送 1-成功赠送 2-不同意赠送
    'TYPE|1':[0,1,2,3,4],//赠送类型  0-售卖库存1-存酒库存 2-过期 4-卡赠 3-美女台赠
    AUDITOR: '@cname',
    'auditor_DEPARTMENT|1':['营销部','市场部','楼面部','演义部','工程部'],
    'DETAILS|1-5':[
      {
        ORDERDETAILS_ID:'@string(32)',
        NAME:'@ctitle(5,20)',
        PAY_DATETIME:'@datetime("MM-dd HH:mm")',
        ORIGINAL_PRICE:'@float(500, 10000, 2, 2)',
        ORDER_MAN:'@cname',
        GOODS_ID:'@string(32)',
        ORDER_DATETIME:'@datetime("MM-dd HH:mm")',
        QUANTITY:'@integer(1, 10)',
        'ORDER_STATE|1':[0,1]//0-待支付 1-已支付 ，这里就是待赠送和已赠送的意思
      }
    ]
  }
}

const unpayedPresentList = {
  status: 'success',
  'data|0-1': [
    {
      NAME:'@ctitle(5,20)',
      PRICE:'@float(500, 10000, 2, 2)',
      QUANTITY:'@integer(1, 10)',
      REQUEST_DATETIME:'@datetime("yyyy-MM-dd HH:mm")',
    }
  ]
}

export {choosePresentList,presentStatus,presentIdList,presentList,presentDetail,unpayedPresentList}
