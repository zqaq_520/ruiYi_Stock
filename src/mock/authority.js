
const authority = {
  status:'success',
  data:{
    'h5_stockHelper':[
      {
        'subscribeManager_stock':[
          'subscribe_manager',
          'subscribe_hostess',
          {'模块2':['add','del','query']}
        ]
      },
      {
        'presentManager_stock':[
          'present_manager',
          'present_girl',
          {'模块2':['add','del','query']}
        ]
      },
      {
        'turnoverStatistics_stock':[
          {'模块1':['add','del','query']}
        ]
      },
      {
        'setting_stock':[
          {'模块1':['add','del','query']},
          {'模块2':['add','del','query']}
        ]
      }
    ]
  }
}

export {authority}
