import Mock from 'mockjs'

const regionList = {
  status: 'success',
  data:['大厅区','吧台区','包厢区']
}

const roomTypeList = {
  status: 'success',
  data:[
    {
      ROOMPLATFORMTYPE_ID:'@string(32)',
      name:'散台<br/>（大厅区）'
    },
    {
      ROOMPLATFORMTYPE_ID:'@string(32)',
      name:'散台<br/>（吧台区）'
    },
    {
      ROOMPLATFORMTYPE_ID:'@string(32)',
      name:'卡座'
    },
    {
      ROOMPLATFORMTYPE_ID:'@string(32)',
      name:'高卡'
    },
    {
      ROOMPLATFORMTYPE_ID:'@string(32)',
      name:'包厢'
    }
  ]
}

const tablesByType = {
  status: 'success',
  data:function(){
    let regionArray = [
        {name:'大厅区',value:'datingqu'},
        {name:'吧台区',value:'bataiqu'},
        {name:'包厢区',value:'baoxiangqu'}
      ],roomTypeArray = [
        {name:'包厢',value:'baoxiang'},
        {name:'高卡',value:'gaoka'},
        {name:'卡座',value:'kazuo'},
        {name:'散台',value:'santai'}
      ],array = [
        {name: '所有区域',value: 'allRegion',parent: 0},
        {name: '所有类型',value: 'allRegion_of_allRoomType',parent: 'allRegion'},
        {name: '所有房台',value: 'allRoom_in_allRegion_of_allRoomType',parent: 'allRegion_of_allRoomType'}
      ]
    for(var i = 0; i < regionArray.length; i++){
      array.push({
        name: regionArray[i].name,
        value: regionArray[i].value,
        parent: 0
      })
      array.push({
        name: '所有类型',
        value: regionArray[i].value + '_of_allroomType',
        parent: regionArray[i].value
      })
      array.push({
        name: '所有房台',
        value: 'allRoom_in_' + regionArray[i].value + '_of_allroomType',
        parent: regionArray[i].value + '_of_allroomType'
      })
      let index = 0,end = roomTypeArray.length
      if(regionArray[i].value == 'baoxiangqu'){index = 0,end = 1}
      if(regionArray[i].value == 'bataiqu'){index = 2}
      if(regionArray[i].value == 'datingqu'){index = 1}
      for(var j = index; j < end; j++){
        array.push({
          name: roomTypeArray[j].name,
          value: roomTypeArray[j].value + '-' + regionArray[i].value,
          parent: regionArray[i].value
        })
        array.push({
          name: '所有房台',
          value: 'allRoom_in_' + regionArray[i].value + '_of_' + roomTypeArray[j].value,
          parent: roomTypeArray[j].value + '-' + regionArray[i].value
        })
        for(var k = 0; k < 10; k++){
          array.push(Mock.mock({
            'name|1': ['A1台','B1台','C1台','M1台','A2台','B2台','C2台','M2台','A3台','B3台','C3台','M3台','1号房'],
            value: '@string(32)',
            parent: roomTypeArray[j].value + '-' + regionArray[i].value
          }))
        }
      }
    }
    return array
  }
}

export {regionList,roomTypeList,tablesByType}
