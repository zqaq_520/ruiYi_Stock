import Mock from "mockjs";

const payMethod = {
  status:'success',
  data:[
    {PAYMODE_ID:'@string(32)',NAME:'现金'},
    {PAYMODE_ID:'@string(32)',NAME:'银行卡'},
    {PAYMODE_ID:'@string(32)',NAME:'支付宝'},
    {PAYMODE_ID:'@string(32)',NAME:'微信'},
    {PAYMODE_ID:'@string(32)',NAME:'会员现金卡'},
    {PAYMODE_ID:'@string(32)',NAME:'会员功勋卡'},
    {PAYMODE_ID:'@string(32)',NAME:'挂账'}
  ]
}

const orderByTable = {
  status:'success',
  data:[
    {ORDER_ID:'@string(32)',ORDER_NUMBER:'20181212100'+'@integer(100000000, 999999999)',ORDER_DATETIME:'2018-12-10 19:30',DRAWER:'李易天'},
    {ORDER_ID:'@string(32)',ORDER_NUMBER:'20181212100'+'@integer(100000000, 999999999)',ORDER_DATETIME:'2018-12-10 20:30',DRAWER:'易伟刚'}
  ]
}

const orderIdList = {
  status:'success',
  'data|19':[
    {
      ORDER_ID:'@string(32)'
    }
  ]
}

const orderList = {
  status:'success',
  'data|10':[
    {
      ORDER_ID:'@string(32)',
      ORDER_NUMBER:'20181212100'+'@integer(100000000, 999999999)',
      ORDER_DATETIME:'@datetime("yy-MM-dd HH:mm")',
      DRAWER:'@cname',
      'SUBSCRIBE_MODE|1':['',1,2,3,4],//1-自助预约 2-销售预约 3-老板预约 4-股东预约
      SUBSCRIBER: '@cname',
      'subscribe_DEPARTMENT|1':['营销部','市场部','楼面部','演义部','工程部'],
      'ROOM_PLATFORM_TYPE|1': ['散台','卡座','高卡','包厢'],
      'AREA|1':['大厅区','吧台区','包厢区'],
      'NUMBER|1':['A1台','B1台','C1台','M1台','A2台','B2台','C2台','M2台','A3台','B3台','C3台','M3台','1号房'],
      CUSTOMER_NAME:'@cname',
      'payed|1':[0,'@float(300, 50000, 2, 2)'],
      'presented|1':[0,'@float(300, 50000, 2, 2)'],
      'IS_BEAUTY|1':[0,1]
    }
  ]
}

const orderDetail = {
  status:'success',
  data:{
    ORDER_ID:'@string(32)',
    ORDER_NUMBER:'20181212100'+'@integer(100000000, 999999999)',
    ORDER_DATETIME:'@datetime("yyyy-MM-dd HH:mm")',
    DRAWER:'@cname',
    open_OPERATOR:'@cname',
    'clear_OPERATOR|1':['','@cname'],
    'SUBSCRIBE_MODE|1':['',1,2,3,4],//1-自助预约 2-销售预约 3-老板预约 4-股东预约
    SUBSCRIBER: '@cname',
    'subscribe_DEPARTMENT|1':['营销部','市场部','楼面部','演义部','工程部'],
    'CONSUMPTION_NUMBER|1': ['','@integer(1, 10)'],
    'ROOM_PLATFORM_TYPE|1': ['散台','卡座','高卡','包厢'],
    'AREA|1':['大厅区','吧台区','包厢区'],
    'NUMBER|1':['A1台','B1台','C1台','M1台','A2台','B2台','C2台','M2台','A3台','B3台','C3台','M3台','1号房'],
    'CUSTOMER_NAME|1':['','@cname'],
    'REMARK|1':['','@cparagraph'],
    total:'@float(300, 50000, 2, 2)',
    payed:'@float(300, 50000, 2, 2)',
    unpay:'@float(300, 50000, 2, 2)',
    discount:'@float(300, 50000, 2, 2)',
    present:'@float(300, 50000, 2, 2)',
    minimumCharge:'@integer(300, 1000)',
    'ORDER_STATE|1':[0,1,2,4],//0-未完成 1-完成 2-取消 4-失败
    'goodDetails|1-5':[
      {
        ORDERDETAILS_ID:'@string(32)',
        NAME:'@ctitle(5,20)',
        PAY_DATETIME:'@datetime("MM-dd HH:mm")',
        'PAY_TYPE|1':[payMethod.data[0].NAME,payMethod.data[1].NAME,payMethod.data[2].NAME,payMethod.data[3].NAME,payMethod.data[4].NAME,payMethod.data[5].NAME,payMethod.data[6].NAME],
        'TYPE|1':[0,1,2,3],//0-下单 1-会员卡赠 2-人赠
        ORIGINAL_PRICE:'@float(500, 10000, 2, 2)',
        'REAL_PRICE|1':[0,'@float(300, 5000, 2, 2)'],
        'MEMBERSHIP_CARD|1':['@string(32)'+'[1万8尊爵卡]','@string(32)'+'[1万金至尊卡]','@string(32)'+'[6千8至尊卡]','@string(32)'+'[3千8星钻卡]','@string(32)'+'[1580畅饮卡]','@string(32)'+'[1100畅饮卡]'],
        ORDER_MAN:'@cname',
        'ORDER_STATE|1':[0,1],//0-待支付 1-已支付
        GOODS_ID:'@string(32)',
        ORDER_DATETIME:'@datetime("MM-dd HH:mm")',
        QUANTITY:'@integer(1, 10)'
      }
    ]
  }
}

export {orderByTable,orderIdList,orderList,orderDetail}
