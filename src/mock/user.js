import Mock from 'mockjs'

const loginUser = {
  userId:'@string(32)',
  workId:'11111',
  password:123456,
  name:'金秀贤',
  phone:13711112222
}

//loginVerify的返回数据格式
const loginSuccuss = {//（成功）
  msg:'登录成功',
  status:'success',
  token:'@string(128)',
  userId:loginUser.userId
}
const loginError = {//（失败）
  msg:'后端返回的错误提示的信息！',
  status:'error',
  token:false,
  userId:''
}

const weiXinAuthStatus = {//weiXinAuthStatus的返回数据格式
  status:'success',
  data:{
    access_token:"15_EArTl2VVRbFjhUHsduaglGM3FIlR3Bz0nBj5N0yUA1BAXoLh7Hn39imoIj04FMaQHQRm_i9W5LhpHtduzvYMmw",
    openid:"o7BcKwKtzeqIkTfLU2KB-UJtaesI",
    refresh_token:"15_ABd_wZg9vV3UsYHWrJ2q_uL0J-pya6h8GaXzd8re4wGkrszQQonyPsZi9a-euhE35NhFOBTtVomSD68B8KGaOA",
    userinfo:{"openid":"o7BcKwKtzeqIkTfLU2KB-UJtaesI","nickname":"明","sex":1,"language":"zh_CN","city":"佛山","province":"广东","country":"中国","headimgurl":"http://thirdwx.qlogo.cn/mmopen/vi_32/jHarwTsjb9wdBEcyUVOl6q8icHxoHy7a03KMySZoqY3FFwyIibnRKrRubjnLSqhDXF7ejDcsFX36aKZT4an9pKNw/132","privilege":[],"userid":"7a689c9dc1e346e1922267d46dd64f18"}
  },
  msg:'后端返回的提示信息'
}

const loginOutStatus = {//loginOut的返回数据格式
  status:function (){return Mock.Random.boolean()?'success':'error'}
}

const userInfo = {//getUserInfo的返回数据格式，判断没有登录等情况则返回false
  status:'success',
  data:{
    JOB_NUMBER:loginUser.workId,
    PHONE:loginUser.phone,
    NAME:loginUser.name
  }
}

const checkCode = {//sendCheckCode的返回数据格式
  status:'success',
  msg:'后端返回的提示信息'
}

const thePassword = {//changePassword的返回数据格式
  status:function (){return Mock.Random.boolean()?'success':'error'},
  msg:'后端返回的提示信息'
}

const checkRegister = {//checkRegister的返回数据格式
  status:'success',
  data:{
    'name|1':['','@cname']
  }
}

export {loginUser,weiXinAuthStatus,loginSuccuss,loginError,loginOutStatus,userInfo,checkCode,thePassword,checkRegister}
