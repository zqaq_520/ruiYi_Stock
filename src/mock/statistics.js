import Mock from 'mockjs'

const turnoverInfo = {
  status:'success',
  data:{
    date:'@datetime("yyyy-MM-dd")',
    total:'200000',
    totalOrder:'@integer(50, 150)',
    perTransaction:'@float(1000, 10000, 2, 2)',
    orderedTurnover:'@float(50000, 200000, 2, 2)',
    chargeTurnover:'@float(50000, 200000, 2, 2)',
    detail: [
      { name: '现金', value: '50000.00' },
      { name: '银行卡', value: '60000.00' },
      { name: '微信', value: '30000.00' },
      { name: '支付宝', value: '60000.00' },
      { name: '现金会员卡', value: '@float(50000, 200000, 2, 2)' },
      { name: '功勋会员卡', value: '@float(50000, 200000, 2, 2)' },
      { name: '现金挂账（已收）', value: '@float(50000, 200000, 2, 2)' },
      { name: '银行卡挂账（已收）', value: '@float(50000, 200000, 2, 2)' },
      { name: '微信挂账（已收）', value: '@float(50000, 200000, 2, 2)' },
      { name: '支付宝挂账（已收）', value: '@float(50000, 200000, 2, 2)' },
      { name: '订台押金', value: '@float(50000, 200000, 2, 2)' },
      { name: '竞拍订金', value: '@float(50000, 200000, 2, 2)' }
    ]
  }
}

export {turnoverInfo}
