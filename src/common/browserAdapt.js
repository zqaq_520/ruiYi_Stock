﻿function open_exe(shellp,str){  
	try{
		a = new ActiveXObject("wscript.shell");  
		a.run(shellp+"   "+str);
	}catch(err){
		alert(err);
	}
}

function turn(url){
	//open_exe('chrome',url)
	window.location.href = url;
}

function loopBrowser(browserList,version){
	for(var i = 0; i < browserList.length; i++){
		if(browserList[i] == version) return true;
	}
	return false;
}
/*
 * browserList是数组，取值可为['IE6','IE7','IE8','IE9','IE10','IE11','Firefox','Netscape','Chrome','Safari','Opera','mobile','IOS','iPad','iPhone','iPod','Android']
 * func为对应浏览器该执行的函数
 * 例子：browserAdapt(['IE6','IE7'],function(browser){window.location.href = "http://www.baidu.com";});
 * */
export function browserAdapt(browserList,func){
	var match = false;
	if(!navigator.userAgent.match(/mobile/i)){//判断为非移动设备   //alert('非移动设备:\n'+navigator.userAgent);
		if(navigator.userAgent.indexOf("MSIE") != -1 || navigator.userAgent.indexOf("Trident") != -1) {//判断依据：IE10及以下都带有MSIE，IE8-11都有Trident，但IE6-7没有Trident  //alert('Internet Explorer:\n'+navigator.userAgent);
			if(navigator.appName == "Microsoft Internet Explorer"){//判断依据：IE6-10的navigator.appName为Microsoft Internet Explorer，而IE11的为Netscape
				if(navigator.appVersion.match(/10./i)=="10."){//alert("IE 10:\n"+navigator.appVersion);
					if(loopBrowser(browserList,'IE10')){if(func != null)func('IE10');match = true;}
				}else if(navigator.appVersion.match(/9./i)=="9."){//alert("IE 9:\n"+navigator.appVersion);
					if(loopBrowser(browserList,'IE9')){if(func != null)func('IE9');match = true;}
				}else if(navigator.appVersion.match(/8./i)=="8."){//alert("IE 8:\n"+navigator.appVersion);
					if(loopBrowser(browserList,'IE8')){if(func != null)func('IE8');match = true;}
				}else if(navigator.appVersion.match(/7./i)=="7."){//alert("IE 7:\n"+navigator.appVersion);
					if(loopBrowser(browserList,'IE7')){if(func != null)func('IE7');match = true;}
				}else if(navigator.appVersion.match(/6./i)=="6."){//alert("IE 6:\n"+navigator.appVersion);
					if(loopBrowser(browserList,'IE6')){if(func != null)func('IE6');match = true;}
				}
			}else{//alert('IE 11:\n'+navigator.userAgent);
				if(loopBrowser(browserList,'IE11')){if(func != null)func('IE11');match = true;}
			}
		}else if(navigator.userAgent.indexOf("Firefox") != -1) {//判断为火狐浏览器  //alert('Firefox:\n'+navigator.userAgent);
			if(loopBrowser(browserList,'Firefox')){if(func != null)func('Firefox');match = true;}
		}else if(navigator.userAgent.indexOf("Netscape") != -1) {//判断为Netscape浏览器  //alert('Netscape:\n'+navigator.userAgent);
			if(loopBrowser(browserList,'Netscape')){if(func != null)func('Netscape');match = true;}
		}else if(navigator.userAgent.indexOf("Chrome") != -1) {//判断为Chrome浏览器  //alert('Chrome:\n'+navigator.userAgent);
			if(loopBrowser(browserList,'Chrome')){if(func != null)func('Chrome');match = true;}
		}else if(navigator.userAgent.indexOf("OPR") != -1) {//判断为Opera浏览器  //alert('Opera:\n'+navigator.userAgent);
			if(loopBrowser(browserList,'Opera')){if(func != null)func('Opera');match = true;}
		}else if(navigator.userAgent.indexOf("Safari") != -1) {//判断为Safari浏览器  //alert('Safari:\n'+navigator.userAgent);
			if(loopBrowser(browserList,'Safari')){if(func != null)func('Safari');match = true;}
		}else{//判断为无法识别的浏览器
			alert('无法识别的浏览器:\n'+navigator.userAgent);
		}
	}else if(navigator.userAgent.match(/mobile/i)) {//判断为移动设备  //alert('移动设备:\n'+navigator.userAgent);
		if(loopBrowser(browserList,'mobile')){//alert('移动设备:\n'+navigator.userAgent);
			if(func != null)func('mobile');match = true;
		}else{
			if(navigator.userAgent.match(/iPad/) || navigator.userAgent.match(/iPhone/) || navigator.userAgent.match(/iPod/)){
				if(loopBrowser(browserList,'IOS')){//alert('IOS设备:\n'+navigator.userAgent);
					if(func != null)func('IOS');match = true;
				}else{
					if(navigator.userAgent.match(/iPad/)){//alert('iPad:\n'+navigator.userAgent);
						if(loopBrowser(browserList,'iPad')){if(func != null)func('iPad');match = true;}
					}else if(navigator.userAgent.match(/iPhone/)){//alert('iPhone:\n'+navigator.userAgent);
						if(loopBrowser(browserList,'iPhone')){if(func != null)func('iPhone');match = true;}
					}else if(navigator.userAgent.match(/iPod/)){//alert('iPod:\n'+navigator.userAgent);
						if(loopBrowser(browserList,'iPod')){if(func != null)func('iPod');match = true;}
					}
				}
			}else if(navigator.userAgent.match(/Android/)){//alert('Android设备:\n'+navigator.userAgent);
				if(loopBrowser(browserList,'Android')){if(func != null)func('Android');match = true;}
			}else{//判断为无法识别的操作系统
				alert('无法识别的操作系统:\n'+navigator.userAgent);
			}
		}
	}else{//判断为无法识别的设备
		alert('无法识别的设备:\n'+navigator.userAgent);
	}
	return match;
}
