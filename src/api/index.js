
import axios from 'axios'
import qs from 'qs'
import {message,getLocalStorage,delLocalStorage} from '../common/util'
import {clearvuex} from "../vuex/clear";
import store from '../vuex'
import router from '../router'
import vm from '../main'

//网站绝对路径配置
export let contextPath = ''
// 微信重定向地址
export let weiXinRedirect = ''

if(window.location.href.indexOf('www.matrix98.com') > 0){
  /*
  真实服务器
  * */
  contextPath = 'https://www.matrix98.com/jhxx_ruiyi/'
  switch (sessionStorage.getItem('index')) {
    case 0:weiXinRedirect = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxe3e017b28e4180b9&redirect_uri=https%3A%2F%2Fwww.matrix98.com%2Fsales%3Findex%3D0&response_type=code&scope=snsapi_userinfo&state=188#wechat_redirect';break
    case 1:weiXinRedirect = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx6e53bec9862ed008&redirect_uri=https%3A%2F%2Fwww.matrix98.com%2Fsales%3Findex%3D1&response_type=code&scope=snsapi_userinfo&state=188#wechat_redirect';break
    case 2:weiXinRedirect = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx8492a220294ac518&redirect_uri=https%3A%2F%2Fwww.matrix98.com%2Fsales%3Findex%3D2&response_type=code&scope=snsapi_userinfo&state=188#wechat_redirect';break
    default:break
  }
}else{
  /*
  本地测试服务器
  * */
  contextPath = 'http://192.168.1.140:9900/jhxx_ruiyi/' //改成想要链接的本地测试服务器路径
  //本地微信重定向路径（啊明）
  switch (sessionStorage.getItem('index')) {
    case 0:weiXinRedirect = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxe3e017b28e4180b9&redirect_uri=https%3A%2F%2Fwww.matrix98.com%2FweixinTest%2Fstock_ming-office.html%3findex%3d0&response_type=code&scope=snsapi_userinfo&state=188#wechat_redirect';break
    case 1:weiXinRedirect = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx6e53bec9862ed008&redirect_uri=https%3A%2F%2Fwww.matrix98.com%2FweixinTest%2Fstock_ming-office.html%3findex%3d1&response_type=code&scope=snsapi_userinfo&state=188#wechat_redirect';break
    case 2:weiXinRedirect = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx8492a220294ac518&redirect_uri=https%3A%2F%2Fwww.matrix98.com%2FweixinTest%2Fstock_ming-office.html%3findex%3d2&response_type=code&scope=snsapi_userinfo&state=188#wechat_redirect';break
    default:break
  }
  //本地微信重定向路径（啊辉）
  /*switch (sessionStorage.getItem('index')) {
    case 0:weiXinRedirect = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxe3e017b28e4180b9&redirect_uri=https%3A%2F%2Fwww.matrix98.com%2FweixinTest%2Fstock_Jason-office.html%3findex%3d0&response_type=code&scope=snsapi_userinfo&state=188#wechat_redirect';break
    case 1:weiXinRedirect = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx6e53bec9862ed008&redirect_uri=https%3A%2F%2Fwww.matrix98.com%2FweixinTest%2Fstock_Jason-office.html%3findex%3d1&response_type=code&scope=snsapi_userinfo&state=188#wechat_redirect';break
    case 2:weiXinRedirect = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx8492a220294ac518&redirect_uri=https%3A%2F%2Fwww.matrix98.com%2FweixinTest%2Fstock_Jason-office.html%3findex%3d2&response_type=code&scope=snsapi_userinfo&state=188#wechat_redirect';break
    default:break
  }*/
  //本地微信重定向路径（啊塱）
  /*switch (sessionStorage.getItem('index')) {
    case 0:weiXinRedirect = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxe3e017b28e4180b9&redirect_uri=https%3A%2F%2Fwww.matrix98.com%2FweixinTest%2Fstock_lang-office.html%3findex%3d0&response_type=code&scope=snsapi_userinfo&state=188#wechat_redirect';break
    case 1:weiXinRedirect = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx6e53bec9862ed008&redirect_uri=https%3A%2F%2Fwww.matrix98.com%2FweixinTest%2Fstock_lang-office.html%3findex%3d1&response_type=code&scope=snsapi_userinfo&state=188#wechat_redirect';break
    case 2:weiXinRedirect = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx8492a220294ac518&redirect_uri=https%3A%2F%2Fwww.matrix98.com%2FweixinTest%2Fstock_lang-office.html%3findex%3d2&response_type=code&scope=snsapi_userinfo&state=188#wechat_redirect';break
    default:break
  }*/
  /*
  使用mock假数据
  * */
  contextPath = ''
  weiXinRedirect  = 'http://192.168.1.173:8085'//改成自己的本地vue运行路径
}

// axios 配置
axios.defaults.timeout = 20000;
axios.defaults.baseURL = contextPath + 'api/stock/'; //这是调用数据接口

// http request 拦截器，通过这个，我们就可以把Cookie传到后台
axios.interceptors.request.use(
  config => {
    const token = getLocalStorage('token_stock_index' + sessionStorage.getItem('index')) //获取Cookie
    const userId = getLocalStorage('userId_stock_index' + sessionStorage.getItem('index')) //获取userId
    //config.data = JSON.stringify(config.data);
    //后台接收的参数
    config.data = qs.stringify(config.data)
    if(config.data != '') config.data += '&'
    config.data += 'accessTypeCode=1&'
    config.data += 'token='+token + '&'
    config.data += 'USER_ID='+userId + '&'
    config.data += 'dbIndex='+ sessionStorage.getItem('index')
    //console.log(config.data)
    config.headers = {
      'Content-Type':'application/x-www-form-urlencoded' //设置跨域头部
    };
    return config
  },
  err => {
    return Promise.reject(err)
  }
)

// http response 拦截器
axios.interceptors.response.use(
  response => {
//response.data.errCode是我接口返回的值，如果值为2，说明Cookie丢失，然后跳转到登录页，这里根据大家自己的情况来设定
    if(response.data.status == 'timeout'
      && location.hash.indexOf('password') < 0//设置某些特别的页面不会跳转
    ) {
      delLocalStorage('token_stock_index' + sessionStorage.getItem('index'))
      delLocalStorage('userId_stock_index' + sessionStorage.getItem('index'))
      clearvuex()
      console.log('interceptors redirect!')
      message({message:'系统超时，请重新进入系统，或关闭页面后重新进入系统',type:'error'})
      router.push({
        path: '/login',
        query: {redirect: router.currentRoute.fullPath}  //从哪个页面跳转
      })
    }
    return response
  },
  error => {
    return Promise.reject(error.response.data)
  })

//网络异常或服务器宕机时候执行的操作
function axiosTimeout() {
  store.commit('updateLoadingStatus',{isLoading:false})
  vm.$vux.confirm.show({
    title:'网络请求异常',
    content:'网络异常，请尝试以下操作，或关闭页面后重新进入系统',
    confirmText:'刷新当前页面',
    cancelText:'返回登录页',
    onCancel () {
      delLocalStorage('token_stock_index' + sessionStorage.getItem('index'))
      delLocalStorage('userId_stock_index' + sessionStorage.getItem('index'))
      clearvuex()
      router.push({path: '/login'})
    },
    onConfirm () {
      router.go(0)
    }
  })
}


export const weiXinAuth = params => {
  let reqParams = {//传到后台的数据
    code:params
  }
  return new Promise((resolve, reject)=>{
    axios.post('weiXinAuth', reqParams).then(res => resolve(res.data)).catch((err) => {reject('weiXinAuth axios err:' + err);axiosTimeout()})
  })
}

export const loginVerify = params => {
  let reqParams = {//传到后台的数据
    username:params.username,
    password:params.password,
    OPENID:params.openId
  }
  return new Promise((resolve, reject)=>{
    axios.post('loginVerify', reqParams).then(res => resolve(res.data)).catch((err) => {reject('loginVerify axios err:' + err);axiosTimeout()})
  })
}

export const loginOut = params => {
  return new Promise((resolve, reject)=>{
    axios.post('loginOut', params).then(res => resolve(res.data)).catch((err) => {reject('loginOut axios err:' + err);axiosTimeout()})
  })
}

export const getUserInfo = params => {
  return new Promise((resolve, reject)=>{
    axios.post('getUserInfo', params).then(res => resolve(res.data)).catch((err) => {reject('getUserInfo axios err:' + err);axiosTimeout()})
  })
}

export const sendCheckCode = params => {
  let reqParams = {//传到后台的数据
    phone:params
  }
  return new Promise((resolve, reject)=>{
    axios.post('sendCheckCode', reqParams).then(res => resolve(res.data)).catch((err) => {reject('sendCheckCode axios err:' + err);axiosTimeout()})
  })
}

export const changePassword = params => {
  let reqParams = {//传到后台的数据
    phone: params.phone,
    checkCode: params.checkCode,
    password: params.password,
    password2: params.password2
  }
  return new Promise((resolve, reject)=>{
    axios.post('changePassword', reqParams).then(res => resolve(res.data)).catch((err) => {reject('changePassword axios err:' + err);axiosTimeout()})
  })
}

export const getAuthority = params => {
  return new Promise((resolve, reject)=>{
    axios.post('getAuthority', params).then(res => resolve(res.data)).catch((err) => {reject('getAuthority axios err:' + err);axiosTimeout()})
  })
}

export const getBusinessHours = params => {
  return new Promise((resolve, reject)=>{
    axios.post('getBusinessHours', params).then(res => resolve(res.data)).catch((err) => {reject('getBusinessHours axios err:' + err);axiosTimeout()})
  })
}

export const getSubscribeTotals = params => {
  return new Promise((resolve, reject)=>{
    axios.post('getSubscribeTotals', params).then(res => resolve(res.data)).catch((err) => {reject('getSubscribeTotals axios err:' + err);axiosTimeout()})
  })
}

export const getSubscribeIdList = params => {
  let reqParams = {//传到后台的数据
    STATE:params.choosenSubscribeStatus[0]
  }
  return new Promise((resolve, reject)=>{
    axios.post('getSubscribeIdList', reqParams).then(res => resolve(res.data)).catch((err) => {reject('getSubscribeIdList axios err:' + err);axiosTimeout()})
  })
}

export const getSubscribeByIds = params => {
  let reqParams = {//传到后台的数据
    SUBSCRIBE_IDS:JSON.stringify(params)
  }
  return new Promise((resolve, reject)=>{
    axios.post('getSubscribeByIds', reqParams).then(res => resolve(res.data)).catch((err) => {reject('getSubscribeByIds axios err:' + err);axiosTimeout()})
  })
}

export const getRoomType = params => {
  return new Promise((resolve, reject)=>{
    axios.post('getRoomType', params).then(res => resolve(res.data)).catch((err) => {reject('getRoomType axios err:' + err);axiosTimeout()})
  })
}

export const checkRegistered = params => {
  let reqParams = {//传到后台的数据
    PHONE:params
  }
  return new Promise((resolve, reject)=>{
    axios.post('checkRegistered', reqParams).then(res => resolve(res.data)).catch((err) => {reject('checkRegistered axios err:' + err);axiosTimeout()})
  })
}

export const submitSubscribe = params => {
  let reqParams = {//传到后台的数据
    subscribe: params.subscribe,//当为普通预订时候值为normal，当为节日/派对预订时候值为HOLIDAYPARTY_ID的值
    //后台应先判断该HOLIDAYPARTY_ID的STATE状态是否能够进行预订，状态不为“预约中”，即返回不成功的信息
    subscribeTime: params.subscribeTime,
    choosenRoomType: params.choosenRoomType,
    PHONE: params.phone,
    name: params.name,
    consumptionNum: params.consumptionNum,
    remark: params.remark
  }
  return new Promise((resolve, reject)=>{
    axios.post('submitSubscribe', reqParams).then(res => resolve(res.data)).catch((err) => {reject('submitSubscribe axios err:' + err);axiosTimeout()})
  })
}

export const getHolidayPartyById = params => {
  let reqParams = {//传到后台的数据
    HOLIDAYPARTY_ID:params
  }
  return new Promise((resolve, reject)=>{
    axios.post('getHolidayPartyById', reqParams).then(res => resolve(res.data)).catch((err) => {reject('getHolidayPartyById axios err:' + err);axiosTimeout()})
  })
}

export const getHolidayPartyList = params => {
  return new Promise((resolve, reject)=>{
    axios.post('getHolidayPartyList', params).then(res => resolve(res.data)).catch((err) => {reject('getHolidayPartyList axios err:' + err);axiosTimeout()})
  })
}

export const getSubscribeById = params => {
  let reqParams = {//传到后台的数据
    SUBSCRIBE_ID:params
  }
  return new Promise((resolve, reject)=>{
    axios.post('getSubscribeById', reqParams).then(res => resolve(res.data)).catch((err) => {reject('getSubscribeById axios err:' + err);axiosTimeout()})
  })
}

export const getDepositAbout = params => {
  let reqParams = {//传到后台的数据
    ROOMPLATFORMTYPE_ID:params
  }
  return new Promise((resolve, reject)=>{
    axios.post('getDepositAbout', reqParams).then(res => resolve(res.data)).catch((err) => {reject('getDepositAbout axios err:' + err);axiosTimeout()})
  })
}

export const getDepositTerm = params => {
  return new Promise((resolve, reject)=>{
    axios.post('getDepositTerm', params).then(res => resolve(res.data)).catch((err) => {reject('getDepositTerm axios err:' + err);axiosTimeout()})
  })
}

export const cancelSubscribe = params => {
  let reqParams = {//传到后台的数据
    SUBSCRIBE_ID:params
  }
  return new Promise((resolve, reject)=>{
    axios.post('cancelSubscribe', reqParams).then(res => resolve(res.data)).catch((err) => {reject('cancelSubscribe axios err:' + err);axiosTimeout()})
  })
}

export const getOrderDetail = params => {
  let reqParams = {//传到后台的数据
    ORDER_ID:params
  }
  return new Promise((resolve, reject)=>{
    axios.post('getOrderDetail', reqParams).then(res => resolve(res.data)).catch((err) => {reject('getOrderDetail axios err:' + err);axiosTimeout()})
  })
}

export const getOrderIdList = params => {
  let reqParams = {//传到后台的数据
    choosenTablesByType:JSON.stringify(params.choosenTablesByType),
    management:params.authority
  }
  return new Promise((resolve, reject)=>{
    axios.post('getOrderIdList', reqParams).then(res => resolve(res.data)).catch((err) => {reject('getOrderIdList axios err:' + err);axiosTimeout()})
  })
}

export const getOrderByIds = params => {
  let reqParams = {//传到后台的数据
    ORDER_IDS:JSON.stringify(params)
  }
  return new Promise((resolve, reject)=>{
    axios.post('getOrderByIds', reqParams).then(res => resolve(res.data)).catch((err) => {reject('getOrderByIds axios err:' + err);axiosTimeout()})
  })
}

export const getChoosePresentList = params => {
  let reqParams = {//传到后台的数据
    type:params
  }
  return new Promise((resolve, reject)=>{
    axios.post('getChoosePresentList', reqParams).then(res => resolve(res.data)).catch((err) => {reject('getChoosePresentList axios err:' + err);axiosTimeout()})
  })
}

export const checkStock = params => {
  let reqParams = {//传到后台的数据
    selectedList:JSON.stringify(params.selectedList),
    TYPE:params.type
  }
  return new Promise((resolve, reject)=>{
    axios.post('checkStock', reqParams).then(res => resolve(res.data)).catch((err) => {reject('checkStock axios err:' + err);axiosTimeout()})
  })
}

export const submitPresent = params => {
  let reqParams = {//传到后台的数据
    ORDER_ID:params.orderId,
    TYPE:params.type,
    selectItems:JSON.stringify(params.selectItems),
    REMARK: params.remark,
    GOODS_IDS:JSON.stringify(params.allGoodIds)
  }
  return new Promise((resolve, reject)=>{
    axios.post('submitPresent', reqParams).then(res => resolve(res.data)).catch((err) => {reject('submitPresent axios err:' + err);axiosTimeout()})
  })
}

export const getPresentedIdList = params => {
  let reqParams = {//传到后台的数据
    choosenDate:params.choosenDate
  }
  return new Promise((resolve, reject)=>{
    axios.post('getPresentedIdList', reqParams).then(res => resolve(res.data)).catch((err) => {reject('getPresentedIdList axios err:' + err);axiosTimeout()})
  })
}

export const getPresentByIds = params => {
  let reqParams = {//传到后台的数据
    PRENSENT_IDS:JSON.stringify(params)
  }
  return new Promise((resolve, reject)=>{
    axios.post('getPresentByIds', reqParams).then(res => resolve(res.data)).catch((err) => {reject('getPresentByIds axios err:' + err);axiosTimeout()})
  })
}

export const getPresentDetail = params => {
  let reqParams = {//传到后台的数据
    PRESENT_ID:params
  }
  return new Promise((resolve, reject)=>{
    axios.post('getPresentDetail', reqParams).then(res => resolve(res.data)).catch((err) => {reject('getPresentDetail axios err:' + err);axiosTimeout()})
  })
}

export const getUnpayedPresent = params => {
  let reqParams = {//传到后台的数据
    area:params
  }
  return new Promise((resolve, reject)=>{
    axios.post('getUnpayedPresent', reqParams).then(res => resolve(res.data)).catch((err) => {reject('getUnpayedPresent axios err:' + err);axiosTimeout()})
  })
}

export const getTablesByType = params => {
  let reqParams = {//传到后台的数据
    managementArea:params
  }
  return new Promise((resolve, reject)=>{
    axios.post('getTablesByType', reqParams).then(res => resolve(res.data)).catch((err) => {reject('getTablesByType axios err:' + err);axiosTimeout()})
  })
}

export const getTurnover = params => {
  return new Promise((resolve, reject)=>{
    axios.post('getTurnover', params).then(res => resolve(res.data)).catch((err) => {reject('getTurnover axios err:' + err);axiosTimeout()})
  })
}
